//
//  Movie+Data.m
//  FLTest
//
//  Created by Patricia Marie Cesar on 12/5/14.
//  Copyright (c) 2014 FreeLancer. All rights reserved.
//

#import "Movie+Data.h"

@implementation Movie (Data)

- (Movie *)initWithDictionary:(NSDictionary *)dictionary
{
	NSString *name = [dictionary objectForKey:kName];
	NSString *start_time = [dictionary objectForKey:kStartTime];
	NSString *end_time = [dictionary objectForKey:kEndTime];
	NSString *channel = [dictionary objectForKey:kChannel];
	NSString *rating = [dictionary objectForKey:kRating];
	
	return [[Movie alloc] initWithName:name startTime:start_time endTime:end_time channel:channel rating:rating];
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.name forKey:kName];
	[encoder encodeObject:self.start_time forKey:kStartTime];
	[encoder encodeObject:self.end_time forKey:kEndTime];
	[encoder encodeObject:self.channel forKey:kChannel];
	[encoder encodeObject:self.rating forKey:kRating];
}

- (id)initWithCoder:(NSCoder *)decoder
{
	if((self = [super init])) {
		self.name = [decoder decodeObjectForKey:kName];
		self.start_time = [decoder decodeObjectForKey:kStartTime];
		self.end_time = [decoder decodeObjectForKey:kEndTime];
		self.channel = [decoder decodeObjectForKey:kChannel];
		self.rating = [decoder decodeObjectForKey:kRating];
	}
	return self;
}

@end

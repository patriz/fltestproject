//
//  DataManager.m
//  FLTest
//
//  Created by Patricia Marie Cesar on 12/5/14.
//  Copyright (c) 2014 FreeLancer. All rights reserved.
//

#import "DataManager.h"
#import "Movie+Data.h"

static DataManager *_instance = nil;

@interface DataManager ()

@property (strong, nonatomic) NSUserDefaults *userDefaults;

@end


@implementation DataManager

@synthesize movieItems = _movieItems;

#pragma mark - Singleton method

+ (DataManager *)sharedManager
{
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[DataManager alloc] init];
	});
	return _instance;
}

- (void)setMovieItems:(NSMutableArray *)movieItems
{
	[[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:movieItems] forKey:@"movieItems"];
}

- (NSMutableArray *)movieItems
{
	NSData *data = [self.userDefaults objectForKey:@"movieItems"];
	
	if (data != nil) {
		
		NSArray *old = [NSKeyedUnarchiver unarchiveObjectWithData:data];
		
		if (old != nil) {
			return old.mutableCopy;
		}
	}
	return nil;
}

- (NSUserDefaults *)userDefaults
{
	if (!_userDefaults) {
		_userDefaults = [NSUserDefaults standardUserDefaults];
	}
	return _userDefaults;
}
@end

//
//  DataManager.h
//  FLTest
//
//  Created by Patricia Marie Cesar on 12/5/14.
//  Copyright (c) 2014 FreeLancer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

@property (strong, nonatomic) NSMutableArray *movieItems;

+ (DataManager *)sharedManager;

@end

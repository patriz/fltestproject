//
//  Movie+Data.h
//  FLTest
//
//  Created by Patricia Marie Cesar on 12/5/14.
//  Copyright (c) 2014 FreeLancer. All rights reserved.
//

#import "Movie.h"

@interface Movie (Data)

- (Movie *)initWithDictionary:(NSDictionary *)dictionary;

- (id)initWithCoder:(NSCoder *)decoder;

- (void)encodeWithCoder:(NSCoder *)encoder;

@end

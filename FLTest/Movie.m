//
//  Movie.m
//  Created by Tao Yang on 24/06/14.
//  Copyright (c) 2013 Tao Yang. All rights reserved.////

#import "Movie.h"

@implementation Movie

- (instancetype)initWithName:(NSString *)mname startTime:(NSString *)mstartTime endTime:(NSString *)mendTime channel:(NSString *)mchannel rating:(NSString *)mrating
{
	self = [super init];
	if (!self) {
		return nil;
	}
	
	Movie *movie = [[Movie alloc] init];
	movie.name = mname;
	movie.start_time = mstartTime;
	movie.end_time = mendTime;
	movie.channel = mchannel;
	movie.rating = mrating;
	
	return movie;
}

@end

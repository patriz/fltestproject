//
//  Constant.h
//  FLTest
//
//  Created by Patricia Marie Cesar on 12/5/14.
//  Copyright (c) 2014 FreeLancer. All rights reserved.
//

#ifndef FLTest_Constant_h
#define FLTest_Constant_h

#define URL @"http://www.whatsbeef.net/wabz/guide.php?start="

#define kName @"name"
#define kStartTime @"start_time"
#define kEndTime @"end_time"
#define kChannel @"channel"
#define kRating @"rating"

#endif

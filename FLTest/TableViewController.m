//
//  TableViewController.m
//  Created by Tao Yang on 24/06/14.
//  Copyright (c) 2013 Tao Yang. All rights reserved.//
//

#import "TableViewController.h"
#import "Movie.h"
#import "ASIHTTPRequest.h"
#import "JSONKit.h"
#import "DataManager.h"
#import "Movie+Data.h"
#import "Reachability.h"

@interface TableViewController ()

@property (nonatomic) int maxRequest;
@property (nonatomic) int currentPage;

@end

@implementation TableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.edgesForExtendedLayout = UIRectEdgeNone;
	self.extendedLayoutIncludesOpaqueBars = NO;
	self.automaticallyAdjustsScrollViewInsets = NO;
	
	self.title = @"FLTest";
	self.currentPage = 0;
	[self requestMovies:self.currentPage];
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Network Call

- (void)requestMovies:(int)page
{
	NSString *stringURL = [NSString stringWithFormat:@"%@%d", URL, page];
    NSURL *url = [NSURL URLWithString:stringURL];
	
	if ([self hasInternetConnection] && (page == 0 || page < self.maxRequest)) {
		__block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
		[request startSynchronous];
		
		NSData *responseData = [request responseData];
		NSDictionary *resultsDictionary = [responseData objectFromJSONData];
		
		self.maxRequest = [[resultsDictionary objectForKey:@"count"] intValue];
		
		NSArray *ms = [resultsDictionary objectForKey:@"results"];
		
		for (NSDictionary *dictionary in ms) {
			
			Movie *movie = [[Movie alloc] initWithDictionary:dictionary];
			[self.movies addObject:movie];
		}
		
		[DataManager sharedManager].movieItems = self.movies;
		[self.tableView reloadData];
	}
	else {
		if ([self hasInternetConnection] && [DataManager sharedManager].movieItems.count) {
			self.movies = [DataManager sharedManager].movieItems;
		}
	}
}

- (BOOL)hasInternetConnection
{
	Reachability *reachability = [Reachability reachabilityForInternetConnection];
	NetworkStatus internetStatus = [reachability currentReachabilityStatus];
	
	
	if (internetStatus != NotReachable) {
		return YES;
	}
	return NO;
}

#pragma mark - IBAction handler

- (IBAction)editButtonClicked:(id)sender
{
	self.tableView.editing = !self.tableView.editing;
	[sender setTitle:(self.tableView.editing) ? @"Done" : @"Edit"];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if([self.movies count] > 0 && [self.movies count] < self.maxRequest) {
//        return [self.movies count] + 1;
//    }
    return [self.movies count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.row == [self.movies count]) {
//        
//        static NSString *CellIdentifier = @"CellIdentifier";
//        
//        // Dequeue or create a cell of the appropriate type.
//        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//        if (cell == nil) {
//            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
//            
//        }
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        UIActivityIndicatorView *spinner = [[[UIActivityIndicatorView alloc]
//                                             initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
//        [spinner startAnimating];
//        cell.accessoryView = spinner;
//        cell.textLabel.numberOfLines = 2;
//        cell.textLabel.font = [UIFont boldSystemFontOfSize:16];
//		cell.textLabel.textColor = [UIColor colorWithRed:0.353 green:0.353 blue:0.353 alpha:1.0];
//        cell.textLabel.text = @"Loading";
//        
//        return cell;
//    } else {
        static NSString *CellIdentifier = @"MovieCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
	cell.backgroundColor = [UIColor clearColor];
        Movie *m = [self.movies objectAtIndex:indexPath.row];
        UILabel *channelLabel = (UILabel *)[cell viewWithTag:100];
        channelLabel.text = m.channel;
		
        UILabel *nameRatingLabel = (UILabel *)[cell viewWithTag:101];
        nameRatingLabel.text = [NSString stringWithFormat:@"%@ (%@)",m.name, m.rating];
        
        UILabel *timeLabel = (UILabel *)[cell viewWithTag:102];
        timeLabel.text = [NSString stringWithFormat:@"%@ - %@",m.start_time, m.end_time];
	
        return cell;
//    }
	
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //do another request
	if (indexPath.row == self.movies.count-1) {
		[self requestMovies:self.currentPage++];
	}

}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		[self.movies removeObjectAtIndex:indexPath.row];
		[self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationBottom];
		[DataManager sharedManager].movieItems = self.movies;
		[self.tableView reloadData];
	}
}

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

#pragma mark - Lazy loading of property

- (NSMutableArray *)movies
{
	if (!_movies) {
		_movies = [[NSMutableArray alloc] init];
	}
	return _movies;
}

@end
